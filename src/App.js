import "./App.css";
import Game from "./components/Game";

function App() {
  return (
    <>
      <div className="container d-flex justify-content-center">
        <Game />
      </div>
    </>
  );
}

export default App;
