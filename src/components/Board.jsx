import React, { useState } from 'react'
import { calculateNextValue, calculateStatus, calculateWinner } from "../logic/Logic";
import Square from './Square';
function Board() {
    const [squares, setSquares] = useState(Array(9).fill(null))

    const nextValue = calculateNextValue(squares)
    const winner = calculateWinner(squares)
    const status = calculateStatus(winner, squares, nextValue)

    const selectSquare = function(square) {
        if (winner || squares[square]) {
            return
        }
        const squaresCopy = [...squares]
        squaresCopy[square] = nextValue
        setSquares(squaresCopy)
    }

    const restart = function() {
        setSquares(Array(9).fill(null))
    }

    function renderSquare(i) {
        return (
            <Square className="square" onClick={() => selectSquare(i)}>{squares[i]}</Square>
        )
    }

    return (
        <div>
            <div className="status">{status}</div>
            <div className="board-row">
                {renderSquare(0)}
                {renderSquare(1)}
                {renderSquare(2)}
            </div>
            <div className="board-row">
                {renderSquare(3)}
                {renderSquare(4)}
                {renderSquare(5)}
            </div>
            <div className="board-row">
                {renderSquare(6)}
                {renderSquare(7)}
                {renderSquare(8)}
            </div>
            <button className="restart btn btn-sm btn-primary" onClick={restart}>
                restart
            </button>
        </div>
    )
}

export default Board